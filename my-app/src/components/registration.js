import React from 'react';
import { useForm } from 'react-hook-form';
import { Form, Button } from 'react-bootstrap';

const FirstStep = (props) => {
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <Form className="input-form" onSubmit={handleSubmit(onSubmit)}>
      <div className="col-md-6 offset-md-3">
        <Form.Group controlId="first_name">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            name="first_name"
            placeholder="Enter your first name"
            autoComplete="off"
            ref={register({
              required: 'First name is required.',
              pattern: {
                value: /^[a-zA-Z]+$/,
                message: 'First name should contain only characters.'
              }
            })}
            className={`${errors.first_name ? 'input-error' : ''}`}
          />
          {errors.first_name && (
            <p className="errorMsg">{errors.first_name.message}</p>
          )}
        </Form.Group>

        <Form.Group controlId="last_name">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            type="text"
            name="last_name"
            placeholder="Enter your last name"
            autoComplete="off"
            ref={register({
              required: 'Last name is required.',
              pattern: {
                value: /^[a-zA-Z]+$/,
                message: 'Last name should contain only characters.'
              }
            })}
            className={`${errors.last_name ? 'input-error' : ''}`}
          />
          {errors.last_name && (
            <p className="errorMsg">{errors.last_name.message}</p>
          )}
        </Form.Group>

        <Form.Group controlId="mob_num">
          <Form.Label>Mobile number</Form.Label>
          <Form.Control
            type="number"
            name="mob_number"
            placeholder="Enter your Mobile number"
            autoComplete="off"
            ref={register({
              required: 'Mobile no is required.',
              pattern: {
                value: /0-9/,
                message: 'only numbers 0-9.'
              }
            })}
            className={`${errors.mob_num ? 'input-error' : ''}`}
          />
          {errors.mob_num && (
            <p className="errorMsg">{errors.mob_num.message}</p>
          )}
        </Form.Group>

        <FormGroup>
         <Label>BirthDate</Label>
           <Input type='date'
           placeholder='Enter BirthDate'
          value={values.birthdate} onChange={handleChange}
         name='birthdate'
             />
              {errors.birthdate && <p className='error'>{errors.birthdate}</p>}
        </FormGroup>
 
        <FormGroup>
          <Label>Address</Label>
           <Input type='textarea'
            placeholder='Enter Address'
            value={values.address} onChange={handleChange}
    
           name='address' />
           {errors.address && <p className='error'>{errors.address}</p>}
         </FormGroup>


         render() {
    return (
      <div>
        <input type="radio" value="Male" name="gender" /> Male
        <input type="radio" value="Female" name="gender" /> Female
        <input type="radio" value="Other" name="gender" /> Other
      </div>
    );
  }
         <Button variant="primary" type="submit">
          submit
        </Button>

      </div>
    </Form>
  );
};

export default FirstStep;